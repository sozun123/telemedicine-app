const {
  DB_HOST: host,
  DB_USER: user,
  DB_PASSWORD: password,
  DATABASE: database,
  DB_PORT: port,
} = require("./config");
const { createConnection } = require("mysql");
const { promisify } = require("util");
const { hash } = require("bcrypt");
const session = require("express-session");
const MySQLStore = require("express-mysql-session")(session);

// creates the database object
const db = createConnection({ host, port, user, password, database });

// simplifies making queries to the database
const query = promisify(db.query).bind(db);

module.exports = {
  db,
  query,

  // this method connects to the database, or exits the process on an error
  connectDB: () => {
    db.connect((error) => {
      if (error) {
        console.error(error);
        process.exit(1);
      } else {
        console.log("Connected to DB");
      }
    });
  },

  // this allows express-session to store sessions in a different MySQL database
  // sessionStore: new MySQLStore({
  //   host,
  //   user,
  //   database,
  //   password,
  // }),
  sessionStore: new MySQLStore({}, db),

  // fetches a user from the database by email
  // if password is true, password is included in the returned user
  // otherwise password is omitted for security
  findUserByEmail: (email, password = true) =>
    query(`SELECT * FROM users WHERE email = ?`, [email]).then((r) => {
      const obj = r[0];
      if (obj && !password && obj.password) delete obj.password;
      return obj;
    }),

  // fetches a user from the database by user ID
  // if password is true, password is included in the returned user
  // otherwise password is omitted for security
  findUserById: (id, password = true) =>
    query(`SELECT * FROM users WHERE id = '${db.escape(id)}'`).then((r) => {
      const obj = r[0];
      if (obj && !password && obj.password) delete obj.password;
      return obj;
    }),

  // creates and inserts a new user and patient with the given information
  insertPatient: async ({
    email,
    password,
    first_name,
    last_name,
    age,
    insurance,
  }) => {
    // verify all parameters are included
    if (!email || !password || !first_name || !last_name || !age)
      throw TypeError("Missing parameters");
    if (typeof insurance === "undefined") insurance = "";

    password = await hash(password, 10); // hash the password for security using bcrypt

    await module.exports.createUser({ email, password, type: "patient" }); // create and store the user object in the database
    const user = await module.exports.findUserByEmail(email, false); // fetch the created user

    // create and store a new patient object in the database
    return query(
      `INSERT INTO patients (user_id, first_name, last_name, age, insurance) VALUES (?, ?, ?, ?, ?)`,
      [user.id, first_name, last_name, age, insurance]
    ).then(
      () => user // return the user object
    );
  },
  // creates and inserts a new user and doctor with the given information
  insertDoctor: async ({ email, password, first_name, last_name, dept }) => {
    // verify all parameters are included
    if (!email || !password || !first_name || !last_name || !dept)
      throw TypeError("Missing parameters");

    password = await hash(password, 10); // hash the password for security using bcrypt

    await module.exports.createUser({ email, password, type: "doctor" }); // create and store the user object in the database
    const user = await module.exports.findUserByEmail(email, false); // fetch the created user

    // create and store a new doctor object in the database
    return query(
      `INSERT INTO doctors (user_id, first_name, last_name, doc_dept) VALUES (?, ?, ?, ?)`,
      [user.id, first_name, last_name, dept]
    ).then(
      () => user // return the user object
    );
  },
  // creates and inserts a new user with the given information
  createUser: ({ email, password, type }) => {
    // verify all parameters are included
    if (!email || !password || !type) throw TypeError("Missing parameters");

    // create and store the user object in the database
    return query(`INSERT INTO users (email, password, type) VALUES (?, ?, ?)`, [
      email,
      password,
      type,
    ]);
  },
};
