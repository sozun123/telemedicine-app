const express = require("express");
const cookieParser = require("cookie-parser");
const {
  PORT: defaultPort,
  SESSION_SECRET: secret,
  SESSION_NAME: name,
} = require("./config/config");
const { connectDB, sessionStore } = require("./config/db");
const session = require("express-session");

const app = express();

// connectDB();
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(
  session({
    //   this stores sessions in a second database
    store: sessionStore,

    secret, // secret to encode cookies

    // this prevents empty sessions from being saved
    saveUninitialized: false,

    name, // name of the cookie
    cookie: {
      // these make the cookie secure to prevent attacks
      secure: false,
      sameSite: true,
      httpOnly: true,

      //   this lets the cookie expire after an hour to follow HIPAA guidelines
      maxAge: 60 * 60 * 1000,
    },
    // these prevent the cookie from expiring while the user is active
    // cookie expires after 60 minutes from the user's last request
    resave: true,
    rolling: true,
  })
);

// middleware
// allows requests to read JSON data from body
app.use(express.json({ extended: false }));
// allows requests to read url encoded data from body
app.use(express.urlencoded({ extended: true }));
// allows requests to read cookies
app.use(cookieParser());

app.use((req, res, next) => {
  // if (req.session.user) res.locals.user = req.session.user;
  res.locals.user = req.session.user;
  next();
});

const PORT = process.env.PORT || defaultPort;

// routes
app.use("/", require("./routes/home"));
app.use("/api/users", require("./routes/users"));
app.use("/api/auth", require("./routes/auth"));

// start the server
app.listen(PORT, () => console.log(`Server is running on ${PORT}`));
