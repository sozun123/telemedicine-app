$.fn.fadeIn = function () {
  $(this)
    .css("opacity", 0)
    .slideDown()
    .animate({ opacity: 1 }, { queue: false });
};
$.fn.fadeOut = function () {
  $(this).css("opacity", 1).slideUp().animate({ opacity: 0 }, { queue: false });
};

const showError = (error) => {
  if (error.param === "notifications") return showEvent(error);
  const input = $(`#${error.param}:visible, #${error.param}-register:visible`);
  if (input.parent().hasClass("has-error")) return;
  input.parent().addClass("has-error");
  input.parent().children(".form-error").fadeOut();
  input
    .after(
      `<p class="form-error" style="display: none;">${error.msg}<button type="button" class="close" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button></p>`
    )
    .next()
    .fadeIn();
};

const showEvent = (event, type = "danger") => {
  const div = $("#notifications").children("div");
  if (event) {
    div
      .removeClass("alert-success")
      .addClass(`alert-${type}`)
      .children("span")
      .text(event.msg);
  } else {
    div
      .removeClass("alert-danger")
      .addClass("alert-success")
      .children("span")
      .text("Successfully signed in!");
  }
  div.fadeIn();
};

$(document).on("click", "#logout", function (e) {
  e.preventDefault();
  $.ajax({
    url: "/api/auth/logout",
    method: "post",
  })
    .done((data) => {
      showEvent(
        { msg: "Signed out successfully", param: "notifications" },
        "success"
      );
      setTimeout(() => {
        window.location.reload();
      }, 500);
    })
    .fail(({ responseJSON: { errors } }) => {
      errors.forEach(showError);
    });
});
