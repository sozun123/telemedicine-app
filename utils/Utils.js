const { SESSION_NAME: name } = require("../config/config");
const { serverError } = require("./errors");

module.exports = {
  authenticate: (req, res, user, msg = "signed in") => {
    if (user.password) delete user.password;
    req.session.user = user;
    return res.json({ msg: "success", op: msg });
  },
  signOut: (req, res, msg = "signed out") => {
    req.session.destroy((err) => {
      if (err) return serverError(res, err);

      res.clearCookie(name).json({ msg: "success", op: msg });
    });
  },
};
